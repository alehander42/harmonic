# harmonic

Provides a way of making code asynchronous without going to callback hell.

## Install

```
gem install harmonic
```

## Usage

```ruby
require 'harmonic'

class DataMine
  include Asyncable
  
  def heavy_computation
    sleep 10
    42
  end
  
  def even_heavier_analysis(data)
    sleep 15
    data * 13.84
  end
  
  async def profit
    computed_data = await { heavy_computation }
    briliant_result = await { even_heavier_analysis(computed_data) }
    puts briliant_result
  end
end

DataMine.new.profit
puts "Profiting.."
puts "This will take some time.."
puts "Just wait for the computation to end."
Asyncable.synchronize
```

## Copyright

Copyright (c) 2014 Stanislav Gatev. See LICENSE.rdoc for further details.
