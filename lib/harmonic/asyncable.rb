module Harmonic
  module Asyncable
    @@async_pool = Thread.pool 8

    def self.included(base)
      base.extend ClassMethods
    end

    module ClassMethods
      @@async_methods = Set.new

      def method_added(method_name)
        if @@async_methods.include? method_name
          @@async_methods.delete method_name
          make_async method_name
          @@async_methods << method_name
        end
      end

      def async(*names)
        names.each do |name|
          if method_defined? name
            make_async name
          else
            @@async_methods << name
          end
        end
      end

      def make_async(name)
        method = instance_method name
        define_method(name) do |*args, &block|
          Asyncable.pool.process do
            method.bind(self).(*args, &block)
          end
        end
      end
    end

    def self.pool
      @@async_pool
    end

    def self.synchronize
      pool.shutdown
    end

    def await(&block)
      Fiber.new { Fiber.yield block.call }.resume
    end
  end
end